# ECR Public

Create an IAM use for [GitLab Runner CI](https://gitlab.com/gitlab-org/gitlab-runner) to use to run
[`ecr-public get-login-password`](https://awscli.amazonaws.com/v2/documentation/api/latest/reference/ecr-public/get-login-password.html).

## Run

1. Create an IAM user running `terrafrom apply` from CI of this project.
1. Login into AWS console and find the [`gitlab-runner-ci` user](https://console.aws.amazon.com/iam/home?region=us-west-2#/users/gitlab-runner-ci).
1. Create an Access key under the [security credentials tab](https://console.aws.amazon.com/iam/home?region=us-west-2#/users/gitlab-runner-ci?section=security_credentials).
1. Update [GitLab Runner CI/CD variables](https://gitlab.com/gitlab-org/gitlab-runner/-/settings/ci_cd) `AWS_ACCESS_KEY_ID` and `AWS_SECRET_ACCESS_KEY` that are used to run `aws ecr-public get-login-password`.
