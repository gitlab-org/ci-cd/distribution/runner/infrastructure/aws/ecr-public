terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 2.70"
    }
  }
}

provider "aws" {
  region = "us-east-1"
}

resource "aws_iam_user" "gitlab-runner-ci" {
  name = "gitlab-runner-ci"
  path = "/ci/"

  tags = {
    group      = "runner"
    use        = "ci"
    managed_by = "https://gitlab.com/gitlab-org/ci-cd/distribution/runner/infrastructure/aws/ecr-public"
  }
}

# Roles taken from
# - https://awscli.amazonaws.com/v2/documentation/api/latest/reference/ecr-public/get-login-password.html (docker login)
# - https://docs.aws.amazon.com/AmazonECR/latest/userguide/repository-policy-examples.html (docker push)
resource "aws_iam_user_policy" "gitlab-runner-ci-role" {
  name = "ci"
  user = aws_iam_user.gitlab-runner-ci.name

  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": [
        "sts:GetServiceBearerToken",
        "ecr-public:GetAuthorizationToken",
        "ecr-public:BatchCheckLayerAvailability",
        "ecr-public:InitiateLayerUpload",
        "ecr-public:UploadLayerPart",
        "ecr-public:CompleteLayerUpload",
        "ecr-public:PutImage"
      ],
      "Effect": "Allow",
      "Resource": "*"
    }
  ]
}
EOF
}
